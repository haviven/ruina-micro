import request from "~/apis"
import {handlerData} from "~/utils/essayUtils"

const baseUrl = "/portal";

// 原接口不含有Node
export async function essayList(data){
    const {public: {staticUrl}} = useRuntimeConfig();
    let response = await request.post(`${baseUrl}/list`, data);
    let responseData = response.data;
    let list = responseData.list;
    for (let i = 0; i < list.length; i++) {
        handlerData(staticUrl , list[i]);
    }
    return response;
}

// 原接口含有Node
export async function essayListHasNode(data){
    const {public: {staticUrl}} = useRuntimeConfig();
    let response = await request.post(`${baseUrl}/listHasNode`, data);
    let responseData = response.data;
    let list = responseData.list;
    for (let i = 0; i < list.length; i++) {
        handlerData(staticUrl , list[i]);
    }
    return response;
}


export async function essayClick(name , params){
    const {public: {staticUrl}} = useRuntimeConfig();
    let response = await request.get(`${baseUrl}/click/${name}` , params);
    let responseData = response.data;
    handlerData(staticUrl ,responseData);
    return response;
}

export async function essayByName(name , params) {
    const {public: {staticUrl}} = useRuntimeConfig();

    let response = await request.get(`${baseUrl}/name/${name}` , params);
    let responseData = response.data;
    handlerData(staticUrl ,responseData);
    return response;
}

export async function nearsEssay(data) {
    let response = await request.post(`${baseUrl}/near` , data);
    return response;
}

export async function searchEssay(searchNames , keyword) {
    return await request.post(`${baseUrl}/search`, {
        searchNames,
        keyword
    });
}
