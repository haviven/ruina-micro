import { ElMessage } from 'element-plus'

const fetch = $fetch.create({
    onRequest({request, options}) {
        const {serverUrl ,public: { apiUrl } } = useRuntimeConfig()
        if (process.server) {
            options.baseURL = serverUrl;
        } else {
            options.baseURL = apiUrl;
        }
        /*!// 设置请求头
          options.headers = options.headers || {}
          options.headers.authorization = '...'
        */
    },
    onResponse({ response , options }) {
        let responseData = response._data;
        let isSuccess = responseData.success;
        let responseCode = responseData.code;
        let responseMessage = responseData.message;
        if (isSuccess) {
            return responseData;
        } else {
            if (responseCode === "400" || responseCode === "500") {
                ElMessage.error(responseMessage);
            }
            return Promise.reject(responseData);
        }
    },
    onResponseError({ response , options }) {
        if (response === null) {
            ElMessage.error('网络连接异常,请稍后再试!');
        } else {
            let handler = responseHandler[response.status];
            if(handler) handler();
            else ElMessage.error('未知错误');
        }
    },
});

const responseHandler = {
    400: (response) => ElMessage.error("参数错误"),
    401: (response) => ElMessage.error("您无权访问"),
    403: (response) => ElMessage.error("拒绝访问"),
    404: (response) => ElMessage.error("请求地址未找到"),
    408: (response) => ElMessage.error("请求超时"),
    500: (response) => ElMessage.error("服务器内部错误"),
    501: (response) => ElMessage.error("服务未实现"),
    502: (response) => ElMessage.error("网关错误"),
    503: (response) => ElMessage.error("服务不可用"),
    504: (response) => ElMessage.error("网关超时"),
    505: (response) => ElMessage.error("HTTP版本不受支持")
}

// 自动导出
export default {
    get: (url, params, options = {}) => fetch(url, {method: "GET", params, ...options}),
    post: (url, body, params, options = {}) => fetch(url, {method: "POST", body, params, ...options}),
    put: (url, body, params, options = {}) => fetch(url, {method: "PUT", body, params, ...options}),
    delete: (url, body, params, options = {}) => fetch(url, {method: "DELETE", body, params, ...options}),
}
