import request from "~/apis"


const baseUrl = "/portal";

export async function message(data) {
    return await request.post(`${baseUrl}/leave/message`, data);
}