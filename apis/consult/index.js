import request from "~/apis"


const baseUrl = "/portal";

export async function consult(data) {
    return await request.post(`${baseUrl}/consult`, data);
}