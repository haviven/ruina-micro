import { defineStore } from 'pinia';

const useAppStore = defineStore('app', {
    state: () => ({
        width: 0,
        menuList: []
    }),
    getters: {
        getWidth: (state) => state.width,
        getMenuList: (state) => state.menuList,
    },
    actions: {
        setWidth(width) {
           this.width = width;
        },
        setMenuList(menuList) {
            this.menuList = menuList;
        },
    }
})

export default useAppStore
