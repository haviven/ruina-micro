import useAppStore from './app'; // 假设counter.js文件在store目录下

export default function useStore(){
    return {
        app: useAppStore()
    }
}
