// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true }, // ssr: false,
  css: [
      '~/assets/style/global.scss',
      '~/assets/style/base.scss'
  ],
    hooks: {
        'pages:extend' (pages) {
            pages.push({
                name: 'index',
                path: '/',
                redirect: '/home'
            });
        }
    },
    runtimeConfig: {
        // 仅在服务器端可用的私钥
        serverUrl: process.env.VUE_APP_NUXT_SERVER_API,
        // 公共中的密钥也将公开给客户端
        public: {
            apiUrl: process.env.VUE_APP_NUXT_API_URL,
            staticUrl: process.env.VUE_APP_NUXT_STATIC_URL
        }
    },
    nitro: {
        devProxy: {
            [process.env.VUE_APP_NUXT_API_URL]: {
                target: "http://localhost:7788",
                prependPath: true,
                changeOrigin: true,
            }
        },
        routeRules: {
            [process.env.VUE_APP_NUXT_API_URL + "/**"]: {
                proxy: 'http://localhost:7788/**'
            }
        }
    },
  modules: [
    '@pinia/nuxt',
    '@element-plus/nuxt'
  ]
})
