export default {
    menus: [{
        name: "HOME",
        link: "/home"
    },{
        name: "ABOUTUS",
        link: "/about-us"
    },{
        name: "PRODUCTS",
        link: "/products",
        detailLink: "/products"
    },{
        name: "NEWS",
        link: "/news",
        detailLink: "/news"
    },{
        name: "ARTICLES",
        link: "/articles",
        detailLink: "/articles"
    }]


}
