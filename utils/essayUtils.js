export function handlerData(staticUrl, data) {
    // 处理扩展JSON
    let otherConfigs = data.otherConfigs;
    if (otherConfigs !== undefined && otherConfigs != null && otherConfigs.trim() !== '') {
        try {
            data.otherConfigs = JSON.parse(otherConfigs);
        } catch (e) {
            console.error('解析JSON字符串出错:' + otherConfigs, e);
        }
    } else {
        data.otherConfigs = {};
    }

    let files = data.files;
    if(files === undefined || files === null) files = [];
    // 处理文件到可解析的路径
    for (let i = 0; i < files.length; i++) {
        let item = files[i];
        item.path = staticUrl + item.path;
    }

    // 处理富文本图片等数据到可解析路径
    if (data.description !== undefined && data.description != null) {
        data.description = data.description.split("${fileCenter}").join(staticUrl);
    }
}

export function getFileByName(data , name) {
    if (data === undefined || data === null) {
        return {};
    }

    let files = data.files;
    if(!files) files = [];
    for (let i = 0; i < files.length; i++) {
        let item = files[i];
        // 如果没有传name，默认返回第一个没有名字的
        if ((name === undefined || name == null) && (item.name === undefined || item.name == null || item.name.trim() === "")) {
            return item;
        }
        if (item.name !== undefined && item.name != null && item.name === name) {
            return item;
        }
    }

    return {};
}

export function getFilesByName(data , name){
    if (data === undefined || data == null) {
        return [];
    }

    let files = data.files;
    let result = [];
    for (let i = 0; i < files.length; i++) {
        let item = files[i];
        if (name === undefined || name == null) {
            if (item.name === null || item.name.trim() === '') {
                result.push(item);
            }
        } else {
            if (item.name === name) {
                result.push(item);
            }
        }
    }
    return result;
}
