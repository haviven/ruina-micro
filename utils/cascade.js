/**
 * 树折叠器(js版)
 *
 * @author Haviven
 * @since 2023.05.12
 *
 * 用法说明：
 *  现有如下数据数组
 *      let sources = [{
 *         id: 1,
 *         parentId: 0,
 *         name: "服饰"
 *       },{
 *         id: 2,
 *         parentId: 0,
 *         name: "电器"
 *       },{
 *         id: 3,
 *         parentId: 1,
 *         name: "衣服"
 *       },{
 *         id: 4,
 *         parentId: 1,
 *         name: "裤子"
 *       },{
 *         id: 5,
 *         parentId: 2,
 *         name: "冰箱"
 *       },{
 *         id: 6,
 *         parentId: 3,
 *         name: "短袖"
 *       }];
 *  要折叠成目标数据:
 *      let targets = [{
 *         id: 1,
 *         name: "服饰",
 *         parentPath: "0",
 *         children: [{
 *           id: 3,
 *           name: "衣服",
 *           parentPath: "0,1",
 *           children: [{
 *             id: 6,
 *             name: "短袖",
 *             parentPath: "0,1,3",
 *           }]
 *         },{
 *           id: 4,
 *           name: "裤子",
 *           children: []
 *         }]
 *       },{
 *         id: 2,
 *         name: "电器",
 *         parentPath: "0",
 *         children: [{
 *           id: 5,
 *           parentPath: "0,2",
 *           name: "冰箱"
 *         }]
 *       }]
 *
 *  可通过如下代码实现
 *  let targets = cascade(sources , item => item.id , item => item.parentId , item => {
 *    return {
 *      id: item.id,
 *      name: item.name,
 *      parentPath: null,
 *      children: null
 *    }
 *  })
 *  .bindChildren((target , children) => target.children = children)
 *  .bindParents((target , parents) => target.parentPath = parents.map(item => item.id).join(","))
 *  .rootNodes();
 *
 * @param sources
 * @param getKey
 * @param getParentKey
 * @param transform 转换函数。不是必须的
 * @returns
 */

export default function cascade(sources, getKey, getParentKey, transform) {

  // 初始化nodes
  let nodes = [];
  sources.forEach(source => {
    nodes.push({
      source: source,
      target: transform ? transform(source) : source,
      key: getKey(source),
      parentKey: getParentKey(source),
      children: [],
      parents: []
    });
  });

  // 初始化rootNodes
  let rootNodes = [];
  nodes.forEach(node => {
    let parentKey = node.parentKey;
    if (parentKey === undefined || parentKey == null) {
      rootNodes.push(node);
      return;
    }

    let parentNode = nodes.find(n => n.key === node.parentKey);
    if (parentNode === undefined) {
      rootNodes.push(node);
      return;
    }

    if (parentNode.children === undefined || parentNode.children == null)
      parentNode.children = [];
    parentNode.children.push(node);
  });

  /**
   * 树的 先根/后根 遍历
   */
  function beforeOrAfterRootTraverse(nodes, isBeforeRootTraverse) {
    let result = [];
    if (nodes === undefined || nodes == null || nodes.length === 0) return result;
    for (let i = 0; i < nodes.length; i++) {
      let node = nodes[i];
      if (isBeforeRootTraverse) {
        result.push(node);
        result.push(...beforeOrAfterRootTraverse(node.children, true));
        continue;
      }
      result.push(...beforeOrAfterRootTraverse(node.children, false));
      result.push(node);
    }
    return result;
  }

  /**
   * 树的下层序/上层序遍历
   */
  function downOrUpTraverse(nodes, isDownTraverse) {
    let result = [];
    let tmpChildren = nodes;
    while (tmpChildren.length > 0) {
      let subChildren = [];
      for (let i = 0; i < tmpChildren.length; i++) {
        let node = tmpChildren[i];
        result.push(node);
        let children = node.children;
        if (children != null) subChildren.push(...children);
      }
      tmpChildren = subChildren;
    }
    if (!isDownTraverse) result = result.reverse();
    return result;
  }

  let result = {
    /**
     * 为转换后的每一项绑定孩子节点
     *
     * @param consumer consumer(target , target[])。其中t是当前节点。t[]为识别到的t的子节点。例如bindChildren((target , children) => target.children = children);
     */
    bindChildren: (consumer) => {
      // 叠入节点的初始化,从根节点依次初始化
      let resultNodes = downOrUpTraverse(rootNodes, false);
      resultNodes.forEach(node => {
        let children = node.children;
        let childrenTarget = [];
        if (children !== undefined) {
          children.forEach(child => {
            childrenTarget.push(child.target);
          });
        }
        consumer(node.target, childrenTarget);
      });

      return result;
    },

    /**
     * 为转换后的每一项绑定父亲节点。父节点是一个数组。是一个从根节点到当前节点最近一个节点的父节点集合
     *
     * @param consumer consumer(target , target[])。其中t是当前节点。t[]为识别到的t的父节点。例如bindParents((target , parent) => target.parent = parent);
     */
    bindParents: (consumer) => {
      nodes.forEach(node => {
        let parents = [];
        let tmp = node;
        while (true) {
          let tmpParentKey = tmp.parentKey;
          if (tmpParentKey === undefined || tmpParentKey == null) break;
          tmp = nodes.find(n => n.key === tmpParentKey);
          if (tmp === undefined) break;

          parents.push(tmp);
        }
        node.parents = parents.reverse();
        let targetParent = node.parents.map(parent => parent.target);
        consumer(node.target, targetParent);
      });
      return result;
    },

    /**
     * 获取跟节点
     */
    rootNodes: () => {
      return rootNodes.map(node => node.target);
    },

    /**
     * 获取叶子节点
     */
    leafNodes: () => {
      let list = [];
      nodes.forEach(node => {
        let children = node.children;
        if (children === undefined || children == null || children.length === 0) {
          list.push(node.target);
        }
      });
      return list;
    },

    /**
     * 获取cascade中的节点列表，设有如下树:
     * A
     *  -A1
     *   -A11
     *    -A111
     *   -A12
     *  -A2
     *  -A3
     *   -A31
     * B
     *  -B1
     *  -B11
     *  -B111
     *
     * @param cascadeTo 定义你要获取元素的顺序<br/>
     *  "BEFORE":   先根遍历树，将得到[A, A1, A11, A111, A12, A2, A3, A31, B, B1, B11, B111]<br/>
     *  "AFTER":    后根遍历树，将得到[A111, A11, A12, A1, A2, A31, A3, A, B111, B11, B1, B]<br/>
     *  "DOWN":     向下层序遍历树，将得到[A, B, A1, A2, A3, B1, A11, A12, A31, B11, A111, B111]<br/>
     *  "UP":       向上层序遍历树，将得到[B111, A111, B11, A31, A12, A11, B1, A3, A2, A1, B, A]<br/>
     *  "DEFAULT":  按照你原始的添加进Cascade元素的顺序输出,例如输入序列时顺序为[A, A1, A2, A3, A11, A12, A111, A31, B, B1, B11, B111]
     */
    nodes: (cascadeTo = "DEFAULT") => {
      let cNodes;
      switch (cascadeTo) {
        case "BEFORE":
          cNodes = beforeOrAfterRootTraverse(rootNodes, true);
          break;
        case "AFTER":
          cNodes = beforeOrAfterRootTraverse(rootNodes, false);
          break;
        case "DOWN":
          cNodes = downOrUpTraverse(rootNodes, true);
          break;
        case "UP":
          cNodes = downOrUpTraverse(rootNodes, false);
          break;
        default:
          cNodes = nodes;
      }

      let list = [];
      cNodes.forEach(n => {
        list.push(n.target);
      });
      return list;
    },

    /**
     * 根据parentKey获取所有子节点
     * @param parentKey parentKey
     */
    nodesByParentKey: (parentKey) => {
      let list = [];
      nodes.forEach(node => {
        if (node.parentKey !== undefined && node.parentKey != null && node.parentKey === parentKey)
          list.push(node.target);
      });
      return list;
    }
  };
  return result;
}
