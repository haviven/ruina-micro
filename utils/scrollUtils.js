Math.easeInOutQuad = function(t, b, c, d) {
    t /= d / 2
    if (t < 1) {
        return c / 2 * t * t + b
    }
    t--
    return -c / 2 * (t * (t - 2) - 1) + b
}

var requestAnimFrame = (function() {
    return /*window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame ||*/ function(callback) { window.setTimeout(callback, 10) }
})()

/**
 * @param {number} start
 * @param {number} to
 * @param {number} duration
 * @param {Function} movingCallback
 * @param {Function} movedCallback
 */
export function scrollTo(start ,to, duration,movingCallback, movedCallback) {
    const change = to - start
    const increment = 20
    let currentTime = 0
    duration = (typeof (duration) === 'undefined') ? 500 : duration
    var animateScroll = function() {
        // increment the time
        currentTime += increment
        // find the value with the quadratic in-out easing function
        var val = Math.easeInOutQuad(currentTime, start, change, duration)
        // move the document.body
        movingCallback(val);
        // do the animation unless its over
        if (currentTime < duration) {
            requestAnimFrame(animateScroll)
        } else {
            if (movedCallback && typeof (movedCallback) === 'function') {
                // the animation is done so lets callback
                movedCallback()
            }
        }
    }
    animateScroll()
}