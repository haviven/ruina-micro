export const checkLength = (minLength , maxLength) => {
    return (rule, value, callback) => {
        if (value !== undefined && value !== null && value !== '') {
            let length = value.length;
            if (length < minLength) {
                return callback(new Error("最少输入" + minLength + "个字符"));
            }
            if (length > maxLength) {
                return callback(new Error("最多输入" + maxLength + "个字符"));
            }
            return callback();
        } else {
            return callback();
        }
    }
}